var vue = new Vue({

	http: {
     
      headers: {
        'X-CSRF-TOKEN': document.querySelector('#token').getAttribute('value')
      }
    },

	el: '#app',

	data: {
		newUser: {
			id: '',
			name:'',
			email:''
		},

		success : false,

		edit : false
	},

	methods:{

		fetchUser: function(){
			this.$http.get('api/users', function(data){
				this.$set('users', data)
			})
		},

		RemoveUser: function(id)
		{
			var ConfirmBox = confirm("Are you sure to deleter it?")
			if (ConfirmBox) this.$http.delete('api/users/' + id)
				this.fetchUser()
		},

		EditUser: function(id)
		{
			var user = this.newUser

			this.newUser = { 
				id: '', 
				name:'',
				email:''
			}

			this.$http.post('api/users/' +id,user,function(data){
				console.log(data)
			})

			this.fetchUser()

			this.edit = false 
		},

		ShowUser: function(id){

			this.edit = true

			this.$http.get('api/users/' + id, function(data){
				this.newUser.id = data.id
				this.newUser.name = data.name
				this.newUser.email = data.email
			})
		},

		AddNewUser:function(){

			var user = this.newUser

			this.newUser = { name:'', email:''}

			this.$http.post('api/users',user)

			self = this
			this.success = true
			setTimeout(function(){
				self.success = false
			}, 1000)
			
			this.fetchUser()
		}

	},

	computed:{
		validation : function()
		{
			return{
				name: !!this.newUser.name.trim(),
			}
		},

		isValid: function(){
			var validation = this.validation
			return Object.keys(validation).every(function(key){
				return validation[key]
			})
		}
	},

	ready: function () {
		this.fetchUser()
	}

});