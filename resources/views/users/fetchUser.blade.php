@extends('layout')

@section('content')
	
	<div id="app">
		<h1>
			Crud
		</h1>

		<div class="alert alert-danger" v-if="!isValid">
			<ul>
				<li v-show="!validation.name">Name field is required</li>
				<li v-show="validation.email">Input a valid email address</li>
			</ul>
		</div>

		<div class="alert alert-success" transition="success" v-if="success">Add new user successful</div>


		<form action="#" @submit.prevent="AddNewUser" method="POST">
			<div class="form-group">
				<label for="name"> Name: </label>
				<input v-model="newUser.name" type="text" id="name" name="name" class="form-control">
			</div>

			<div class="form-group">
				<label for="name"> email: </label>
				<input v-model="newUser.email" type="text" id="email" name="email" class="form-control">
			</div>
			<div class="form-group">
				<button :disabled="!isValid" class="btn btn-default" type="submit" v-if="!edit" >Add</button>
			</div>

			<div class="form-group">
				<button :disabled="!isValid" class="btn btn-default" type="submit" v-if="edit" @click="EditUser(newUser.id)">Edit User</button>
			</div>
		</form>

		<table class="table">
			<thead>
				<th>ID</th>
				<th>Name</th>
				<th>Email</th>
				<th>Action</th>
			</thead>

			<tbody>
				<tr v-for="user in users">
					<td>@{{ user.id }}</td>
					<td>@{{ user.name }}</td>
					<td>@{{ user.email }}</td>
					<td>
						<button class="btn btn-default btn-sm" @click="ShowUser(user.id)"> Edit</button>
						<button class="btn btn-danger btn-sm" @click="RemoveUser(user.id)"> Delete</button>
					</td>
				</tr>
			</tbody>
		</table>
	</div>


@endsection

@push('scripts')
<script src="{{url('/js/script.js')}}"></script>

<style>
	.success-transition{
		transition: all .5s ease-in-out;
	}
	.success-enter, .success-leave{
		opacity: 0;
	}
</style>
@endpush
