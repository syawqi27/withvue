<html>
	<head>
	<meta name="token" id="token" value="{{ csrf_token() }}">
		<title>
			CRUDE with Vue
			
		</title>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
	</head>
	
	<body>
		<div class="container">
			@yield('content')
		</div>
		

		
	
	</body>
	<script src="{{url('/js/vendor.js')}}"></script>
	@stack('scripts')
</html>